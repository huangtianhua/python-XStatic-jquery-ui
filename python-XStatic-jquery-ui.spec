%global _empty_manifest_terminate_build 0
Name:           python-XStatic-jquery-ui
Version:        1.13.0.1
Release:        1
Summary:        jquery-ui 1.13.0 (XStatic packaging standard)
License:        MIT
URL:            http://jqueryui.com/
Source0:        https://files.pythonhosted.org/packages/cb/e8/20e39283a6b44299c39f99ab3240662bddccf0ec8d8f8657eddeb0eed93d/XStatic-jquery-ui-1.13.0.1.tar.gz
BuildArch:      noarch
%description
jquery-ui javascript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package -n python3-XStatic-jquery-ui
Summary:        jquery-ui 1.13.0 (XStatic packaging standard)
Provides:       python-XStatic-jquery-ui
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
%description -n python3-XStatic-jquery-ui
jquery-ui javascript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package help
Summary:        jquery-ui 1.13.0 (XStatic packaging standard)
Provides:       python3-XStatic-jquery-ui-doc
%description help
jquery-ui javascript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%prep
%autosetup -n XStatic-jquery-ui-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-XStatic-jquery-ui -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed May 18 2022 OpenStack_SIG <openstack@openeuler.org> - 1.13.0.1-1
- Upgrade package python3-XStatic-jquery-ui to version 1.13.0.1

* Sat Jan 30 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
